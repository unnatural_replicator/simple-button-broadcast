#include <cmath>
#include <vector>
#include "WiFi.h"
// #include "WiFiClient.h"
#include "AsyncUDP.h"
#include "Ticker.h"

#include <HX711_ADC.h>

// da desactivar por chango (pone `false` en vez de `true`)
#define PROG_TEST true  


///// NO TOCAR ESTE //////////////////////////////////////////////
/**/ #ifndef PROG_TEST              // no tocar                 //
/**/ #define PROG_TEST false        // no tocar   ___ (~ )( ~)  //
/**/ #endif                         // no tocar  /   \_\ \/ /   //
/**/ #if PROG_TEST                  // no tocar |   D_ ]\ \/    //
/**/ #include "secrets.h"           // no tocar |   D _]/\ \    //
/**/ #endif                         // no tocar  \___/ / /\ \   //
/**/ #define ADC1_CH0 36            // no tocar       (_ )( _)  //
/**/ #define NO_TRIMMER_UMBRAL 255  // no tocar                 //
//////////////////////////////////////////////////////////////////


// CONFIGURACIONES ///////////////////////////////////////
// configuraciones conexion
#if !PROG_TEST      // credenciales WIFI chango
const char *ssid = "FrochCC";
const char *pass = "frochcorp";
#endif
const bool DATOS_TOUCHDESIGNER = true; // envia directamente en formato que va a entender el touch designer
const uint16_t port                         = 1234;
const IPAddress server_ip                   = IPAddress{192, 168, 4, 1};
const uint32_t send_button_status_every_ms  = 300;      // envía por UDP cada X millisegundos
const uint32_t connection_timeout_ms        = 12000;    // resetea después de X millisegundos que no hay conexión
// config basculas
const std::vector<uint8_t> scale_pins       {0,2,4,15}; // pines DAT o DOUT de las balanzas en orden de numero de balanza
const uint8_t   clock_pin                   = 10;       // pin de clock (CLK) compartido por todos los HX711
double          umbral                      = 0.0;      // umbral predefinido. usar este valor fijo o definir un pin de trimmer
const uint8_t   umbral_trimmer_pin          = 36;       // pin para el trimmer de ajuste del umbral, si es NO_TRIMMER_UMBRAL, no se va a leer y se utiliza el valor predefinido
// const uint8_t   gain                     = 32;       // tiene que ser 32, 64 o 128, no utilizado ahora
const bool      revez                       = true;     // algunos modeles de chip dan un output al revez
const uint8_t   tare_button_pin             = 10;       // botón de tara, con resistór PULLDOWN


// Objectos globaes, NO TOCAR////////////////////////////////
                                    // TIMERS
Ticker send_via_udp;                // para enviár datos por UDP
Ticker print_info_timer;            // para escribir en el puerto Serial las info
Ticker wifi_disconnection_timer;    // para resetear si se desconecta
AsyncUDP udp;                       // para conexión UDP
std::vector<HX711_ADC*> scales;     // array de objectos de las balanzas
const uint8_t analog_read_resolution_bits = 12; // para leer el trimmer del umbral
const double analog_read_max_value = std::pow(2,analog_read_resolution_bits) - 1; // para leer el trimmer del umbral
uint32_t loop_counter = 0;          // para sacar cuantos ciclos ha habido desde el ultimo report por Serial


// saca el ID unico de la estación, por ejemplo `A1E822`
// lo sca desde los ulimos 3 numeros del MAC
String get_station_id()
{
    static String id;
    if (not id.length())
    {
        id = WiFi.macAddress().substring(9);
        id.replace(":", "");
    }
    log_d("id: %s", id.c_str());
    return id;
}
const String station_id = get_station_id();


// envia datos por UDP
void send_presence()
{   
    String buffer;
    if (DATOS_TOUCHDESIGNER) 
        // envia datos de cada sensor uno por línia por ejemplo
        // A1E822-1 1
        // A1E822-2 0
        // A1E822-3 1
        for (int i=0; i<scales.size(); i++){
            buffer.concat(station_id);
            buffer.concat("-");
            buffer.concat(i+1);
            buffer.concat(" ");
            buffer.concat(scales[i]->getData()>umbral?"1":"0");
            buffer.concat("\n");
        }
    else // envia todo en una línia, por ejemplo A1E822 0 0 1 para ahorrar un 30% de datos
        {
            buffer = station_id;
            for (auto scale : scales){ 
                buffer.concat(" ");
                buffer.concat(scale->getData()>umbral?"1":"0");
            }
            buffer.concat("\n");
        }
    log_d("UDP -> %s ", buffer.c_str());
    byte bytes_buffer[128];
    buffer.getBytes(bytes_buffer, 128);
    udp.write(bytes_buffer, buffer.length()); // envía por UDP
}


// Emprime info por el puerto Serial
void print_info()
{
    Serial.println("--BEGIN REPORT--");
    Serial.printf("UDP PIEZO HX711_ADC\t %s\n",  station_id.c_str());
    // Serial.printf("esp id:\t\t %s\n", get_station_id().c_str());
    Serial.printf("wifi:\t\t '%s' %s\n", ssid, (WiFi.status()==WL_CONNECTED)?"connected":"NOT CONNECTED");
    Serial.printf("local ip:\t %s\n", WiFi.localIP().toString().c_str());
    Serial.printf("server:\t\t %s:%u\n", server_ip.toString().c_str(), port);
    // Serial.printf("TCP keepalive:\t %s\n", tcp.connected()?"connected":"not connected");
    Serial.printf("uptime:\t\t %u sec\n", millis() / 1000);
    Serial.printf("temperature:\t %.2f\n", temperatureRead());
    Serial.printf("memory free %:\t %.2f\n", 100* (double)ESP.getFreeHeap() / (double)ESP.getHeapSize());
    Serial.printf("loops:\t\t %u\n", loop_counter); loop_counter=0;
    Serial.printf("threshold:\t %.2f\n", umbral);
    Serial.printf("scales (%u):\n", scales.size());
    for (int i = 0; i<scales.size(); i++){
        Serial.printf("\t#%u on data pin %u is %s (value %u)\n", 
                        i+1 , scale_pins[i], scales[i]->getData()>umbral ? "ON" : "OFF ", scales[i]->getData());
    }
    Serial.println("--END REPORT--");
    Serial.println();
}


void connect_wifi(){
    WiFi.mode(WIFI_STA);
    if (WiFi.begin(ssid, pass) == WL_CONNECT_FAILED){ 
        // si me da directamente WL_CONNECT_FAILED, significa que la radio wifi del ESP ha fallado
        Serial.println("Cannot connect to WiFi. \nSomething wrong with the ESP. \nRestarting...");
        ESP.restart();}

    // aquí ya se que la radio es OK (no la conexión, pero funciona)
    Serial.printf("Connecting to WiFi SSID: %s\n", ssid);
    
    //timer de conexión: resetea si no se conecta en `connection_timeout_ms` (ahora 12s)
    Ticker connection_timeout_timer; 
    connection_timeout_timer.attach_ms(connection_timeout_ms, []()
                                       {
        Serial.println("\nWiFi connection Failed. Restarting");
        delay(10);
        ESP.restart(); });

    // espera la conexión
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(100);
        Serial.print(".");
    }
    // ya estoy conectado, elimina el timer de conexión 
    connection_timeout_timer.detach(); 
    Serial.println("WiFi connected");


    // WiFi disconnection control and restart
    WiFi.onEvent(  
        // en caso que se desconecte, espera unos segundos y, si no se reconecta, resetea
        [](arduino_event_t *event)
        {
            Serial.printf("WiFi Disconnected.\nRestarting in %u sec if cannot reconnect.\n", connection_timeout_ms/1000);
            if (not wifi_disconnection_timer.active())
                wifi_disconnection_timer.attach_ms(connection_timeout_ms, [](){
                    Serial.println("WiFi disconnection timeout! Restarting...");
                    delay(10);
                    ESP.restart();}); 
        },
        ARDUINO_EVENT_WIFI_STA_DISCONNECTED);
    
    WiFi.onEvent(
        // cuando se reconecta, para el timer para resetear
        [](arduino_event_t *event)
        {
            Serial.println("WiFi Reconnected.");
            wifi_disconnection_timer.detach(); 
        },
        ARDUINO_EVENT_WIFI_STA_CONNECTED);

    // wifi a la maxima potencia
    WiFi.setSleep(WIFI_PS_NONE);
    WiFi.setTxPower(WIFI_POWER_19_5dBm);
}

// setup de la conexión UDP
void connect_udp(){
    if (udp.connect(server_ip, port)) // ver si está disponible el socket interno
    {
        Serial.println("UDP connected");
    }
    else // este es un error del modulo radio del ESP
    {
        Serial.println("Cannot activate UDP interface.\nRestarting.");
        delay(10);
        ESP.restart();
    }
}

// setup balanzas
void setup_scales(){
    // espera hasta un segundo desde el arranque y inicializa las balanzas (para dar tiempo al HX711)
    auto delay_ms = (millis()<1000?1000-millis():1);
    log_d("Begin initialising %u scales in %u ms", scale_pins.size(), delay_ms);
    delay(delay_ms);
    for (size_t i=1; (i-1)<scale_pins.size(); i++){ // ciclo para cada balanza
        log_d("Initialising scale #%u", i); delay(5);
        auto data_pin = scale_pins[i-1];      // saca el pin de datos
        log_d("Creating scale #%u on data pin %u, clock pin %u", i, data_pin, clock_pin);
        auto cell = new HX711_ADC(data_pin, clock_pin); // crea el objecto de balanza (puntadór)
        log_d("Begin scale #%u", i);
        cell->begin();                                  // inicializa este objecto (hay un -> porqué es un puntador)
        log_d("Starting scale #%u", i);
        cell->startMultiple(600);                       // arranca la balanza con 1 segundo de tiempo para estabilizarse
        if (revez) { // algunos HX711 dan un valór al revez
            log_d("Setting reverse output for scale %u", i);
            cell->setReverseOutput();
            }
        log_d("Adding scale %u to array", i);
        scales.push_back(cell);                         // añade el puntador al array de balanzas
    }
}


// operación de tara a la presión de un botón
void setup_tare_button(){
    attachInterrupt(digitalPinToInterrupt(tare_button_pin),[](){
        log_d("Tare operation started");
        for (auto cell : scales)
            cell->tareNoDelay();
    }, RISING);
}

void tare_command_udp(AsyncUDP &udp){
    udp.onPacket([](AsyncUDPPacket packet) {
        if (packet.remoteIP() != server_ip) return;
        auto text = String((char*)packet.data());
        if (text.indexOf("tare") > 0 and text.indexOf("tare") < 3)
            for (auto scale : scales)
                scale->tareNoDelay();
        });
}

void setup()
{
    setCpuFrequencyMhz(80); // 80 = ahorro de energía de la CPU. Poner 240 para el máximo (pero aquí es inutil)
                            // la radio WIFI ya está al máximo de la potencia
    Serial.begin(115200);
    delay(20);
    Serial.println("Begin MAIN program");
    

    connect_wifi();

    // conexión UDP y setup de los mensajes periodicos (para enviar el estado de los botones)
    connect_udp();

    // setup balanzas
    setup_scales();

    // Print Serial info about the ESP
    print_info();
    print_info_timer.attach_ms(2000, print_info);

    // UMBRAL / threshold trimmer
    log_d("Setting analog resolution to %u", analog_read_resolution_bits);
    analogReadResolution(analog_read_resolution_bits);

    // empieza a enviár datos por UDP
    send_via_udp.attach_ms(send_button_status_every_ms, send_presence);

}

void loop()
{
    loop_counter++;
    // lee el valor del UMBRAL desde el trimmer si hay
    if (umbral_trimmer_pin != NO_TRIMMER_UMBRAL)
        umbral = analogRead(umbral_trimmer_pin)/analog_read_max_value;

    // leer Valores balanzas (no los envías directamente, los envías en el método send_presence())
    // lee muchos valores y guarda una media
    for (auto cell : scales){ // ciclo para cada balanza
        // log_d("Reading scale");
        if (cell->dataWaitingAsync())
            cell->updateAsync();
    }
  }

