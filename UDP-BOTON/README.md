# UDP button

El ESP envía continuamente (2/3 veces cada segundo) el estado del botón al servidor via UDP(default 1234). El servidor agrega estas informaciones y envía continuamente los datos agregados en un otro puerto UDP (default 1235). 

El sketch de arduino es `UDP.ino` y tiene parte del codigo en la carpeta `arduino-src`

El servidor de python es `UDP.py` y tiene parte del codigo en la carpeta `py-src`. Más abajo hay las instrucciones para arrancarlo

Las otras carpetas/files son de sistema.

## Arrancár el servidor de python

en el `cmd` de windows:

1. ir a la carpeta UDP.py con `cd .\sillas\UDP` (depende de donde haya guardado la carpeta)

1. arrancár el servidor con `py UDP.py`

## Diagostico

El ESP envía un resumen en el puerto seriál cada 2 segundos, y esto causa un relampagár regular del led de comunicación cada 2 segundos, entonces también sin enchufarlo se puede ver si está encendido o no.

El servidor python controla que 