from time import monotonic
from functools import singledispatchmethod
from typing import Dict, List

class Client:
    keepalive:float #seconds
    ip:str
    _last_ping_received:float = -10000000.0 #seconds
    _button_pressed:bool

    def __init__(self, ip:str, button_state:bool=False, keepalive_s:float=5.0):
        self.keepalive = keepalive_s
        self.ip = ip
        self._button_pressed = button_state
    # get the client status (up/down)
    def is_online(self) -> bool:
        return (monotonic() - self._last_ping_received) < self.keepalive
    
    def last_seen_on(self) -> float:
        return self._last_ping_received

    def seen_seconds_ago(self) -> float:
        return monotonic() - self._last_ping_received 

    # notify that the client is still up
    def seen(self, button:bool) -> None:
        self._button_pressed = button
        self._last_ping_received = monotonic()

    # true if is still online and last state received was true
    def is_pressed(self) -> bool:
        return self._button_pressed and self.is_online()

    def __eq__(self, other:object) -> bool:
        if not isinstance(other, Client):
            return False 
        return self.ip == other.ip


class ClientRegister:
    
    keepalive:float
    _register:Dict[str,Client] = dict() # dictionary in which the key and value are equal objects but updating only the second one
    def __init__(self, keepalive:float=5.0) -> None:
        self.keepalive=keepalive
    
    # get ips
    def get_online(self) -> List[str]:
        return [ip for ip, client in self._register.items() if client.is_online()]

    # get ips
    def get_offline(self) -> List[str]:
        return [ip for ip, client in self._register.items() if not client.is_online()]
    
    # get ips
    def get_all(self) -> List[str]:
        return [ip for ip, _ in self._register.items()]

    def get_buttons_state(self, pressed:bool) -> List[str]:
        return [ip for ip, client in self._register.items() if client.is_pressed() == pressed]

    @singledispatchmethod
    def register(self, x) -> None:
        return
    
    @register.register
    def _(self, client:Client) -> None:
        self._register[client.ip] = client

    @register.register
    def _(self, ip:str) -> None:
        self._register[ip] = Client(ip, keepalive_s=self.keepalive)

    @singledispatchmethod
    def is_online(self, x) -> bool:
        return False
    
    @is_online.register
    def _(self, client:Client) -> bool:
        try:
            return self._register[client.ip].is_online()
        except KeyError:
            return False
    
    @is_online.register
    def _(self, ip:str) -> bool:
        try:
            return self._register[ip].is_online()
        except KeyError:
            return False

    # tell that you've just seen the client being online
    # registers it if it is not present in the register 
    @singledispatchmethod
    def update(self, x) -> None:
        return
    @update.register
    def _(self, client:Client, button:bool) -> None:
        try:
            self._register[client.ip].seen(button)
        except KeyError:
            client.seen(button=button)
            self._register[client.ip] = client
    @update.register
    def _(self, ip:str, button:bool) -> None:
        try:
            self._register[ip].seen(button)
        except KeyError:
            new_client:Client = Client(ip, keepalive_s=self.keepalive)
            new_client.seen(button=button)
            self._register[ip] = new_client


    
    @singledispatchmethod
    def remove(self, x) ->None:
        return

    @remove.register
    def _(self, client:Client):
        try:
            del self._register[client.ip]
        except KeyError:
            return 

    @remove.register
    def _(self, ip:str) -> None:
        try:
            del self._register[ip]
        except KeyError:
            return 
