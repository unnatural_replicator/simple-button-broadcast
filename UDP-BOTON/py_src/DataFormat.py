from typing import Callable, Dict, List

class DataWriter:
    _format:str 
    write:Callable[[Dict[str,List[bool]]],bytes]
    separator:str
    buttons:int
    
    # dataformat: csv or line (comma separated values or line-oriented)
    def __init__(self, dataformat:str='csv', value_separator:str=",", buttons_per_station:int=3):
        self._format = dataformat
        self.buttons = buttons_per_station
        self.separator = value_separator
        if dataformat.lower() == 'csv':
            self.write = self._write_csv
        else:
            self.write = self._write_line

    def _write_csv(self, data:Dict[str,List[bool]]) -> bytes:
        ids:List[str] = list(data.keys())
        button_ids:List[str] = []
        for i, id_ in enumerate(ids):
            button_index:str = str((i % self.buttons) + 1) # count from 1
            button_ids.append(id_+"-"+button_index)
        output_str:str = self.separator.join(button_ids)
        output_str += "\n"
        for _, values in data.items():
            output_str += self.separator.join(values)
            output_str += self.separator            
        output_str += "\n"
        return output_str.encode()

    def _write_line(self, data:Dict[str,List[bool]]) -> bytes:
        output_str:str=""
        for key, values in data.items():
            output_str += key + self.separator + self.separator.join([value for value in values]) + "\n"
        return output_str.encode()