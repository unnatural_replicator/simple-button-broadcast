"""agrega los estados de los botones y de los esp y los envia por udp"""

import socket
import asyncio
# from json import dumps
from typing import Dict,List, Union

from py_src.ClientRegister import ClientRegister
from py_src.DataFormat import DataWriter

#TODO translate the data dictionary in a format good for the DataWriter
#TODO argparse
#TODO logging
#TODO extender para theremin (recibe float [0-1))


async def send_status_messages(register:ClientRegister, udp_socket:Union[socket.socket,None]=None, udp_ip:str='localhost', udp_port:int=1235, dataformat:str='csv', send_report_every:float=5.0) -> None: 
    # maximum 2 level deep dictionary (see below to send mqtt)
    message_dict:Dict[str, Union[int,Dict[str,List[str]]]] = dict()
    ids:Dict[str, List[str]] = dict()

    writer:DataWriter = DataWriter(dataformat=dataformat)

    while True:
        await asyncio.sleep(send_report_every)
        # collect info in json and send
        
        online:List[str] = register.get_online()
        online_qty:int = len(online)
        offline:List[str] = register.get_offline()
        offline_qty:int = len(offline)
        buttons_pressed:List[str] = register.get_buttons_state(True)
        buttons_pressed_qty:int = len(register.get_buttons_state(False))
        buttons_not_pressed:List[str] = register.get_buttons_state(False)
        buttons_not_pressed_qty:int = len(buttons_not_pressed)

        if udp_socket:        
            message_dict.clear()
            ids.clear()
            ids['online'] = online
            ids['offline'] = offline
            ids['on'] = buttons_pressed
            ids['off'] = buttons_not_pressed
            
            message_dict['id'] = ids
            message_dict["on"] = buttons_pressed_qty
            message_dict["off"] = buttons_not_pressed_qty
            message_dict["online"] = online_qty
            message_dict["offline"] = offline_qty

                
            try:
                message:bytes = writer.write(message_dict)
            except Exception as e:
                print(e.args[0])
                print("Cannot encode message")
                continue

            try:
                udp_socket.sendto(message, (udp_ip, udp_port))
            except Exception as e:
                print("Cannot send UDP message with %s payload" % dataformat)
                print(e.args[0])
            else:
                # print("Sent json: ")
                # print(message_json)
                pass

def get_button_state(udp_data:bytes) -> bool:
    data_word_list:List[str] = udp_data.decode().lower().split()
    try:
        return bool(int(data_word_list[0]))
    except ValueError:
        return False


class UDPReceiver(asyncio.DatagramProtocol):
    def __init__(self):
        super().__init__()

    def connection_made(self, transport) -> None: 
        self.transport = transport

    def datagram_received(self, data, addr) -> None:
        print("Datagram received")
        try:
            state = get_button_state(data)
        except ValueError:
            return
        else:
            esp_register.update(addr[0], state)


def main(
        udp_receive_on_port:int=1234,
        udp_send_to_ip:str='127.0.0.1',
        udp_send_to_port:int=1235,
        send_report_every:float=0.5,
        dataformat:str='csv',
        keepalive:float=5.0):

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    esp_register = ClientRegister(keepalive=keepalive)
    sock.sendto("SERVER 1".encode(), (udp_send_to_ip, udp_send_to_port))

    loop = asyncio.get_event_loop()
    udp_receiver_task = loop.create_datagram_endpoint(UDPReceiver, local_addr=('0.0.0.0', udp_receive_on_port))
    loop.run_until_complete(udp_receiver_task) 
    loop.run_until_complete(send_status_messages(esp_register, sock,udp_send_to_ip, udp_send_to_port, dataformat=dataformat, send_report_every=send_report_every)) 
    loop.run_forever()


if __name__ == '__main__':
    main()
