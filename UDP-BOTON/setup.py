setup(
    name="button-broadcast-udp-server",
    version="0.1.0",
    description="Receives button statud from ESPs and aggregates the info",
    long_description=README,
    long_description_content_type="text/markdown",
    url="https://codeberg.org/unnatural_replicator/simple-button-broadcast/src/branch/master/UDP",
    author="Menna",
    author_email="",
    license="GPLv3",
    classifiers=[
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+) ",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
    ],
    packages=["py-src"],
    include_package_data=True,
    install_requires=[
        "paho.mqtt"
    ],
    entry_points={"console_scripts": ["button-broadcast-udp-server=py-src.main:main"]},
)