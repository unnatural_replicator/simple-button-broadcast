# Ejemplo de datos enviados

Cada dato ocúpa una línia distinta, y es formado de 
un id del ESP-botón, 
un espacio y 
un numéro `0` o `1` a segunda del estado del botón, `1` significa apretado.

Los id se pueden leer desde el puerto seriál y son los últimas 3 bytes del MAC, una barra `-` y el numero de botón empiezando a contar por `1`.

Un ejemplo de datos por 3 ESP, el primero con todos los botones apretados, el segundo ningun apretado, y el tercero solo tiene el primer boton apretado
```
1A3F22-1 1
1A3F22-2 1
1A3F22-3 1

3E4529-1 0
3E4529-2 0
3E4529-3 0

55E1AA-1 1
55E1AA-2 0
55E1AA-3 0
```

Los espacios sin datos solo son para visibilizar los diferentes ESP, no existerán en los datos enviados

En alternativa se puede resumir en un formado más compacto con id del ESP y en orden el estado de los botones. Los mismos datos de arriba se podrían escribir:
```
1A3F22 1 1 1
3E4529 0 0 0
55E1AA 1 0 0
```

Estos formados alternativos, en respecto al `csv`, son más fácil de analizar por código "custom". Eligir el `csv` si ya hay un nudo para esto, y si no eligir este. 

## Ejemplo de código de python para hacer el parsing
para dár una idea para implementarlo en touchdesigner
```python
# datos recibidos por UDP
datos = paquete_udp.decode()  #"decode" para transformarlos desde `bytes` en caracteres

for línia in datos: # leer cada línia

    estados = línia.split() #separar los datos de cada línia donde hay espacios, produciendo entonces una lista

    nombre = estados[0] # el primero dato es el nombre
    botón1 = bool(estados[1]) # y aquí se convierten en bool para representár el estado del botón
    botón2 = bool(estados[2]) # bool(0) es False, y bool(1) es True
    botón3 = bool(estados[3])
    # ... y hacer algo con estas informaciones
```