/* BUTTON 
ESP-NOW+UDP server/Gateway
*/

#include <vector>
#include <map>
#include "WiFi.h"
#include "WiFiClient.h"
// #include "AsyncUDP.h"
#include "Ticker.h"

#include "InputDebounce.h"
#include "SimpleEspNowConnection.h"

SimpleEspNowConnection *esp_now; // the best RSSI will become the "server/gateway"

#define PROG_TEST true      // carga las info de wifi para pruebas, da desactivar por chango


/////////////////////////////////////////////////////
// configuraciones conexion
const bool DATOS_TOUCHDESIGNER{true}; // envia directamente en formato que va a entender el touch designer
const uint16_t port = 1234;
const IPAddress server_ip = IPAddress{192, 168, 4, 1};
const uint32_t send_button_status_every_ms = 300;
const uint32_t connection_timeout_ms = 5000;
// config boton
const std::vector<uint8_t> scale_pins{12,14,27}; // pines de los botones en orden de numero de botón
const auto pin_mode = InputDebounce::PinInMode::PIM_EXT_PULL_DOWN_RES; // PIM_INT_PULL_UP_RES = internal pullup resistor 
const auto button_type = InputDebounce::SwitchType::ST_NORMALLY_OPEN;
const uint32_t button_debounce_ms = 50;
const uint32_t button_pressed_duration = 0; 


/////////////////////////////////////////////////////
// global objects, do not touch
#if PROG_TEST
#include "secrets.h"
#else
const char *ssid = "FrochCC";
const char *pass = "frochcorp";
#endif
Ticker send_via_udp;
Ticker print_info_timer;
Ticker wifi_disconnection_timer;
Ticker tcp_keepalive_timer;
AsyncUDP udp;
WiFiClient tcp;
std::vector<InputDebounce*> buttons;
wifi_event_id_t connection_callback_handle;
wifi_event_id_t disconnection_callback_handle;


void send_presence()
{   
    
    String buffer;
    if (DATOS_TOUCHDESIGNER)
        for (int i=0; i<buttons.size(); i++){
            buffer.concat(get_station_id());
            buffer.concat("-");
            buffer.concat(i+1);
            buffer.concat(" ");
            buffer.concat(buttons[i]->process(millis())>0?"1":"0");
            buffer.concat("\n");
        }
    else // sendiing A1E822 0 0 1 because of network performance
        {
            buffer = get_station_id();
            for (auto btn : buttons){ 
                buffer.concat(" ");
                buffer.concat(btn->process(millis())>0?"1":"0");
            }
            buffer.concat("\n");
        }

    byte bytes_buffer[128];
    buffer.getBytes(bytes_buffer, 128);
    udp.write(bytes_buffer, buffer.length());
}


String get_station_id()
{
    static String id;
    if (not id.length())
    {
        id = WiFi.macAddress().substring(9);
        id.replace(":", "");
    }
    return id;
}

// station info over serial
void print_info()
{
    Serial.printf("UDP BUTTON\t %s\n",  get_station_id().c_str());
    // Serial.printf("esp id:\t\t %s\n", get_station_id().c_str());
    Serial.printf("wifi:\t\t '%s' %s\n", ssid, (WiFi.status()==WL_CONNECTED)?"connected":"NOT CONNECTED");
    Serial.printf("local ip:\t %s\n", WiFi.localIP().toString().c_str());
    Serial.printf("server:\t\t %s:%u\n", server_ip.toString().c_str(), port);
    Serial.printf("TCP keepalive:\t %s\n", tcp.connected()?"connected":"not connected");
    Serial.printf("uptime:\t\t %u sec\n", millis() / 1000);
    Serial.printf("temperature:\t %.2f\n", temperatureRead());
    Serial.printf("memory free %:\t %.2f\n", 100* (double)ESP.getFreeHeap() / (double)ESP.getHeapSize());
    Serial.printf("buttons:\n");
    for (int i = 0; i<buttons.size(); i++){
        Serial.printf("\t#%u on pin %u is %s\n", 
                        i+1 , buttons[i]->getPinIn(), buttons[i]->process(millis())>0 ? "ON" : "OFF ");
    }
    Serial.println();
}

void connect_wifi(){
    // conexión wifi
    WiFi.mode(WIFI_STA);
    if ((WiFi.begin()== WL_CONNECT_FAILED) or (WiFi.begin(ssid, pass) == WL_CONNECT_FAILED)){
        Serial.println("Cannot connect to WiFi. \nSomething wrong with the ESP. \nRestarting...");
        ESP.restart();}
    Serial.printf("Connecting to WiFi SSID: %s\n", ssid);
    
    Ticker connection_timeout_timer;
    connection_timeout_timer.attach_ms(connection_timeout_ms, []()
                                       {
        Serial.println("\nWiFi connection Failed. Restarting");
        delay(10);
        ESP.restart(); });
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(100);
        Serial.print(".");
    }
    connection_timeout_timer.detach();
    Serial.println("WiFi connected");


    // WiFi disconnection control and restart
    disconnection_callback_handle = WiFi.onEvent(
        [](arduino_event_t *event)
        {
            Serial.printf("WiFi Disconnected.\nRestarting in %u sec if cannot reconnect.\n", connection_timeout_ms/1000);
            if (not wifi_disconnection_timer.active())
                wifi_disconnection_timer.attach_ms(connection_timeout_ms, [](){
                    Serial.println("WiFi disconnection timeout! Restarting...");
                    delay(10);
                    ESP.restart();}); 
            // remove self
            // WiFi.removeEvent(disconnection_callback_handle);
        },
        ARDUINO_EVENT_WIFI_STA_DISCONNECTED);
    
    connection_callback_handle = WiFi.onEvent(
        [](arduino_event_t *event)
        {
            Serial.println("WiFi Reconnected.");
            wifi_disconnection_timer.detach(); 
        },
        ARDUINO_EVENT_WIFI_STA_CONNECTED);

}

// returns the server's mac address or an empty String on error
String negotiate_esp_now_role(){

    // limited 
    using std::map;
    using std::make_pair;

    // listen to others
    auto esp_now_temp_connection = SimpleEspNowConnection(SimpleEspNowRole::SERVER);
    map<String, String> peers_precedence; //RSSI+mac:mac
    String negotiation_message_header = "#";
    esp_now_temp_connection.onMessage([negotiation_message_header, &peers_precedence](uint8_t* address, const uint8_t* message, size_t len){
        if (message[0] != negotiation_message_header) return;
        auto peer_mac = esp_now->macToStr(address);
        uint8_t peer_rssi;
        auto is_valid_negotiation_message = sscanf((char*)message, "%s %09u",negotiation_message_header, &peer_rssi );
        if (is_valid_negotiation_message) {
            peers_precedence.insert(make_pair(peer_mac, peer_rssi));
        }

    });
    
    auto my_rssi = WiFi.RSSI();
    
    uint8_t my_mac[6];
    WiFi.macAddress(my_mac);
    // peers_precedence.insert(make_pair(_mac, peer_rssi));
    // auto my_rssi_s = String(my_rssi); //TODO should be done over a couple seconds, taking the mean
    auto channel = WiFi.channel();
    String broadcast_address_esp_now = "FF:FF:FF:FF:FF:FF";
    // take a temporary role
    char send_message_buffer[5];
    sprintf(send_message_buffer, "%s %03u%03u%03u%03u%03u%03u%03u",negotiation_message_header, my_rssi,my_mac[0], my_mac[1], my_mac[2], my_mac[3], my_mac[4], my_mac[5] );
    esp_now_temp_connection.sendMessage(send_message_buffer);

    // give some seconds for the negotiation to take place
    delay(5000);

    // get max rssi (and associated address) from the map
    String server_mac;
    if (not peers_precedence.empty())
        return peers_precedence.rbegin()->second;
    else
        return ""; // no messages received

}


void setup()
{
    setCpuFrequencyMhz(80);
    Serial.begin(115200);

    connect_wifi();

    negotiate_esp_now_role();


    // UDP activation
    if (udp.connect(server_ip, port))
    {
        Serial.println("UDP connected");
        send_presence();
        send_via_udp.attach_ms(send_button_status_every_ms, send_presence);
    }
    else
    {
        Serial.println("Cannot activate UDP interface.\nRestarting.");
        delay(10);
        ESP.restart();
    }

    // TCP to keepalive connection
    if (tcp.connect(server_ip, port, 3000)){
        tcp.setNoDelay(true);
        tcp_keepalive_timer.attach_ms(1500, [](){tcp.write("");tcp.flush();});
    }


    // Buttons
    for (int i=0; i<scale_pins.size(); i++){
        auto pin = scale_pins[i];
        buttons.push_back(new InputDebounce(pin, button_debounce_ms, pin_mode, button_pressed_duration, button_type));
    }

    // Serial info
    print_info();
    print_info_timer.attach_ms(2000, print_info);
}

void loop()
{

}
