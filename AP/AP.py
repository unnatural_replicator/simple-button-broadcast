#!/bin/env python3

"""Button Server
Access Point version

Counts and report unique SSIDs and their button state

First argument is port [default 1234], second argument is network interface to scan for SSIDs [default 'wlan0']

"""

from wifi import Cell
from wifi.exceptions import InterfaceError
from sys import argv, exit, stderr
from time import sleep
from socket import socket, AF_INET, SOCK_DGRAM
from json import dumps
import platform
import time

### arguments
help_text:str = """Usage:
    python3 AP.py [port [, network_interface]]

    `port` defaults to `1234`
    `network_interface` defaults to `wlan0` on linux, to `en0` on mac and to `wireless_0` on windows
    to know the interface name run
        [on linux]
        `ip link`
        [on windows] 
        `netsh int ipv4 show interfaces`
        [on mac]
        idk? probably `ifconfig -a` 
"""



if len(argv)>3:
    print("Too many arguments.")
    print(help_text)
    exit(-1)

port:int = 1234
if len(argv)>1:
    if argv[1].lower() in ["--help", "-h"]:
        print(help_text)
        exit(0)
    try:
        port = int(argv[1])
        if not (1024 < port < 2**16-1):
            raise ValueError
    except ValueError:
        print("Port is not a valid number")

default_wifi_interface:dict[str,str]={
    'linux':'wlan0',
    'windows':'wireless_0',
    'darwin':'en0'
}

try:
    wifi_interface:str = default_wifi_interface[platform.system().lower()]  
except KeyError:
    wifi_interface = ""
if len(argv)>2:
    wifi_interface = argv[2]
    
if not wifi_interface:
    print("Interface not supported")
    exit(-1)

### program
# udp socket
sock = socket(AF_INET, SOCK_DGRAM)
# register of all the SSIDs discovered so far, even the ones that are no longer visible
buttons_discovered = set() 

# could have been done with actual MAC addresses
def get_base_ssids(ssids:list[str])->set[str]:
    """get unique SSIDs, ignoring the suffix"""
    return set([ssid.rsplit('_',1)[0] for ssid in ssids])

try:
    print("Begin program")
    while True:
        # search for ssids and count the On and the OFF
        try:
            ssid_buttons = [cell.ssid for cell in Cell.where(wifi_interface, lambda cell: cell.ssid.startswith('BTN_'))]
        except InterfaceError:
            print("WiFi interface not existing or turned off. Exiting")
            exit(-2)
        buttons_on = list(filter(lambda ssid: ssid.endswith("ON"), ssid_buttons))
        buttons_off = list(filter(lambda ssid: ssid.endswith("OFF"), ssid_buttons))
        
        buttons_discovered.update(get_base_ssids(ssid_buttons))

        # compile json and send
        message:dict[str, int|list[str]] = {}
        message["total"] = len(buttons_discovered)
        message["ssid"] = list(buttons_discovered)
        message["on"] = len(buttons_on)
        message["off"] = len(buttons_off)
        message["lost"] = message["total"] - (message["on"] + message["off"])

        message_json = dumps(message)
        print()
        print(time.time())
        print(message_json)
        sock.sendto(bytes(message_json, "utf-8"), ('localhost', port))

        sleep(0.5)

except KeyboardInterrupt:
    print("Exiting program.")
    exit(0)

except Exception as e:
    print("Unhandled error happened, trying to continue:", file=stderr)
    print(e, file=stderr)

