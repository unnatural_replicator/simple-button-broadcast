/* BUTTON

Publishes the included button state using its own SSID, using that
as an connectionless unidirectional communication method

The AP SSID will be "BTN_{last 3 mac bytes in hexadecimal}_" 
and "ON" or "OFF" to indicate the button state
i.e. "BTN_1A3F22_ON"
*/

#include "Ticker.h"
#include "WiFi.h"
#include "secrets.h"


/////////////////////////////////////////////////////
// configuraciones
#define USE_SERIAL true
const bool USING_EXTERNAL_PULLUP = false;
const bool USING_NC_BUTTON = false; // NC: normally closed
const uint8_t pin_mode = INPUT_PULLDOWN;
const uint8_t button_pin{12}; // igual poner una resistencia externa de pulldown. Ya hay interna, pero no se cuanto es fuerte
const uint32_t button_delay_ms{1500}; // histeresis
const uint8_t wifi_channel{1};
// el ssid se asigna automaticamente


/////////////////////////////////////////////////////
// objectos globales, no configurar
String AP_ssid; 
const bool reversed_logic = (pin_mode == INPUT_PULLUP) or USING_NC_BUTTON or USING_EXTERNAL_PULLUP;
const String ssid_prefix{"BTN_"};
const String person_present_ssid_suffix{"ON"};
const String person_absent_ssid_suffix{"OFF"};
Ticker button_timer;
bool person_present=false;
bool person_present_old=false;
#if USE_SERIAL
Ticker print_info_timer;
#endif //USE_SERIAL
#define SERIAL if(serial)

// simply wait an read another time
bool read_button(uint8_t pin, uint32_t delay_ms=50){
    if (bool(digitalRead(pin))==0 ^ reversed_logic) 
        return 0;
    delay(delay_ms);
    return bool(digitalRead(pin)) ^ reversed_logic;
}

// get unique SSID (BTN_ + hex mac + _)
String get_base_ssid(){
    String ssid = ssid_prefix;

    ssid.concat( WiFi.macAddress().substring(9));
    ssid.concat("_");
    ssid.replace(String(":"), String(""));
    return ssid;
}

String get_current_ssid(){
    auto ssid = get_base_ssid();
    ssid.concat(person_present ? person_present_ssid_suffix : person_absent_ssid_suffix);
    return ssid;
}

void send_presence(bool is_present){
    WiFi.softAPdisconnect();
    WiFi.enableAP(false);
    WiFi.enableAP(true);
    WiFi.softAP(get_current_ssid(), password, wifi_channel);
    #if USE_SERIAL
    Serial.printf("### ---\n### -> btn changed to %s\n### ---\n", is_present?"ON":"OFF");
    #endif //USE_SERIAL

}

void print_info(){
    #if USE_SERIAL
    Serial.println("### ESP BUTTON AP MODE");
    Serial.printf("### button %spressed\n", person_present?"":"NOT ");
    Serial.printf("### wifi ssid: %s (don't connect to that)\n", get_current_ssid().c_str());
    Serial.printf("### wifi channel: %u\n", wifi_channel);
    Serial.printf("### uptime: %u sec\n", millis()/1000);
    Serial.printf("### button pin: %u\n", button_pin);
    Serial.println("\n");
    #endif //USE_SERIAL
}

void setup(){
    #if USE_SERIAL
    Serial.begin(115200);
    delay(10);
    #endif //USE_SERIAL

    pinMode(button_pin, pin_mode);

    WiFi.setTxPower(WIFI_POWER_19_5dBm); //max trnsmission power
    WiFi.enableAP(true);    

    // setting initial SSID
    send_presence(person_present);

    // sending info on serial port
    #if USE_SERIAL
    print_info();
    print_info_timer.attach_ms(2000, [](){print_info();});
    #endif //USE_SERIAL
}


void loop(){
    auto button_state = read_button(button_pin);

    if (button_state){
        person_present = true;
        button_timer.detach();
        button_timer.attach_ms<bool*>(button_delay_ms, [](bool* person_present){
            *person_present=false;
        }, &person_present);
    }

    if (person_present != person_present_old){
        send_presence(person_present);
        person_present_old = person_present;
    }
}