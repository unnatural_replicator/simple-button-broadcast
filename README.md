# simple-button-broadcast

Broadcast del estado de un botón con varios métodos.

Cada método está en su carpeta y consite en un sketch de Arduino con el nombre de la carpeta con la extensión `.ino` y de un script en python con el mismo nombre y extensión `.py`.

El servidŕ agrega las informaciones de los ESP y envía continuamente JSON por UDP al puerto `1235` compilado de manera similar al `EJEMPLO.json`. El suporte de MQTT *para la publicación de las info agregadas* no es implementado en todos los scripts.

Cada herramienta está en su carpeta y tiene un proprio `README`.

## Lista de herramientas:

- `AP`: cada ESP actua de Access Point wifi y envía el estado del botón cambiando su proprio SSID (nombre de red wifi). Funciona sin ninguna conexión pero es un poco más despacio a la hora de actualizar el estado del botón.

- `MQTT`: cada ESP pública el estado del botón en el servidor MQTT (`mosquitto`), el servidór de `python` coje estas info, las agregas y la republicas por `UDP` y por `MQTT`  

- `UDP` Cada ESP envía por UDP al servidor python su estado y el servidór agrega y republica por UDP y MQTT 

- `ESP-NOW`: work in progress. esp-now es un protocól wifi sin conexión. necesita de porlomenos un "gateway": un esp conectado a una red y que comunique con el router wifi


// esto es un test
