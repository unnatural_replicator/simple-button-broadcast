#!/usr/bin/env bash

shopt -s extglob

# declare -A flags=()
# files=() exprs=()

while (( $# > 0 )) ; do
  case $1 in
    -v|--verbose) (( VERBOSE=1 )) ;;
    # -b|--batch) (( flags[b]++ )) ;;
    # -c|--commit) (( flags[c]++ )) ;;
    # -e|--expr) exprs+=( "${2?Missing argument for -e|--expr}" ) ; shift ;;
    # -f|--file) files+=( "${2?Missing argument for -f|--file}" ) ; shift ;;
    --) shift; break ;;
    -*) printf >&2 'Unknown option %s\n' "$1" ; exit 1 ;;
    *) break ;;
  esac
  shift
done

# declare -p flags exprs files
printf 'ignoring args: %s\n' "${*@Q}"

echo "Press CTRL+C to stop"

echo "[ MOSQUITTO ] Starting mosquitto"
if [[ "$VERBOSE" == 1 ]]; then
    sleep 0.3
    mosquitto --verbose --config-file ./mosquitto.conf & 
else
    mosquitto --config-file ./mosquitto.conf & 
fi

echo "[  PYTHON   ] Starting server"
python MQTT.py
echo "[  PYTHON   ] Server stopped"



