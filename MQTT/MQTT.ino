/* BUTTON MQTT
*/

#include "Ticker.h"
#include "WiFi.h"
#include "EspMQTTClient.h"
#include "secrets.h"

#define TEST false

/////////////////////////////////////////////////////
// configuraciones
// const char* mosquitto_address = "192.168.43.245";
const char* mosquitto_address = "192.168.1.1";
const uint16_t mosquitto_port = 1883;
const bool USING_EXTERNAL_PULLUP = false;
const bool USING_NC_BUTTON = false; // NC: normally closed
const uint8_t pin_mode = INPUT_PULLDOWN;
const uint8_t button_pin{12}; 
const uint32_t button_delay_ms{1500}; // histeresis
const String publish_topic = WiFi.macAddress().substring(9); 
const String mosquitto_clientname = publish_topic; 
const char* person_present_message = "true";
const char* person_absent_message = "false";
const char* last_will_message = "off";
const char* message_on_boot = "false";


/////////////////////////////////////////////////////
// variables globales 
String AP_ssid; 
const bool reversed_logic = (pin_mode == INPUT_PULLUP) or USING_NC_BUTTON or USING_EXTERNAL_PULLUP;
Ticker button_timer;
bool person_present=false;
bool person_present_old=false;
EspMQTTClient mqtt(ssid, password, mosquitto_address, mosquitto_clientname.c_str(), mosquitto_port);
#if TEST_MENNA
Ticker test;
#endif

// simply wait an read another time, it's ok-ish bc it's a pullup or pulldown
bool read_button(uint8_t pin, uint32_t delay_ms=50){
    if (bool(digitalRead(pin))==0 ^ reversed_logic) 
        return 0;
    delay(delay_ms); 
    return bool(digitalRead(pin)) ^ reversed_logic;
}

void send_presence(bool is_present){
    const char* message = person_present?person_present_message:person_absent_message;
    // Serial.printf("Publishing '%s' to topic '%s'\n", message, publish_topic);
    mqtt.publish(publish_topic, message, true);
}

void onConnectionEstablished(){
    // Serial.println("MQTT connection established");
    // Serial.printf("Publishing '%s' to topic '%s'\n", message_on_boot, publish_topic);
    mqtt.publish(publish_topic, message_on_boot,true);
}

void setup(){
    Serial.begin(115200);
    Serial.println("BEGIN MAIN PROGRAM");
    
    pinMode(button_pin, pin_mode);

    mqtt.enableDebuggingMessages();
    
    mqtt.enableLastWillMessage(publish_topic.c_str(), last_will_message, true);
    mqtt.enableDrasticResetOnConnectionFailures();
    mqtt.setKeepAlive(5);
    mqtt.setWifiReconnectionAttemptDelay(3000);
    mqtt.setMqttReconnectionAttemptDelay(2000);

    #if TEST
    test.attach_ms(2500, [](){person_present = not person_present;});
    #endif
}


void loop(){
    auto button_state = read_button(button_pin);

    if (button_state){
        person_present = true;
        button_timer.detach();
        button_timer.attach_ms<bool*>(button_delay_ms, [](bool* person_present){
            *person_present=false;
        }, &person_present);
    }
    mqtt.loop();
    if (person_present != person_present_old){
        Serial.printf("The button became %spressed\n", person_present?"":"not " );
        send_presence(person_present);
        person_present_old = person_present;
    }
}