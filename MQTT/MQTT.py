from paho.mqtt.client import Client, MQTTv311, MQTTMessage
# from sys import argv, exit, stderr
from time import sleep
from socket import socket, AF_INET, SOCK_DGRAM
from json import dumps
from sys import stderr

mosquitto_address = "localhost"
mosquitto_port = 1883
udp_port = 1234
buttons_state:dict[str,bool] = {} # station name and button state, only online stations
esp_register:set[str] = set() # id of every esp ever seen 
# udp socket
sock = socket(AF_INET, SOCK_DGRAM)

# connect to mosquitto 
mqtt = Client(  client_id="servidor",
                protocol=MQTTv311,
                clean_session=False)


def onMessage(c:Client, user_data, message:MQTTMessage)->None:
    if message.topic.startswith("sta"):
        try:
            station = message.topic.split('/')[1].lower()
        except IndexError:
            return
        esp_register.add(station)
        msg_txt:str = message.payload.decode("ascii").lower()
        if  msg_txt == "off":
            try:
                del buttons_state[station]
            except KeyError:
                return
        else:
            buttons_state[station] = msg_txt == "true"


mqtt.on_message = onMessage

try:
    mqtt.connect(mosquitto_address)
    mqtt.subscribe([('sta/#', 1)])
    message:dict[str, int|list[str]] = {}
    # message_old:dict[str, int|list[str]] = {}

    while True:
        sleep(0.2)
        if not esp_register:
            continue

        # compile json and send
        message.clear()
        message["esp online id"] = str(list(buttons_state.keys()))
        message["esp offline id"] = str(list(esp_register.difference(buttons_state.keys())))
        message["esp online quantity"] = len(list(buttons_state.keys()))
        message["esp offline quantity"] = len(list(esp_register.difference(buttons_state.keys())))
        
        message["on"] = len([button for button, state in buttons_state.items() if state])
        message["off"] = len([button for button, state in buttons_state.items() if not state])

        try:
            message_json = dumps(message)
        except:
            print("Cannot encode json message")
            continue

        try:
            sock.sendto(bytes(message_json, "utf-8"), ('localhost', udp_port))
        except:
            print("Cannot send UDP message with JSON payload")

        try:
            for topic, message in message.items():
                mqtt.publish(topic, message)
        except:
            print("Cannot publish on all the topics")


except KeyboardInterrupt:
    print("Exiting program.")
    exit(0)

except Exception as e:
    print("Unhandled error happened, trying to continue:", file=stderr)
    print(e, file=stderr)

