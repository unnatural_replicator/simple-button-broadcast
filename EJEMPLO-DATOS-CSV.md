# Ejemplo de datos enviados

La forma generál es la de un file `csv`. Probablemente ya hay un nudo de TouchDesigner para elaborar `csv`. Los datos de este `csv` son separados por espacios, pero se puedon separar por commas si sale mejór en touchdesigner.

La primera linia son los id de los botones en forma de MAC del ESP - numero botón, p.e. el botón `3` del ESP `1A3F22` será `1A3F22-3`. Los botones se cuentan empiezando por `1`, entonces el primer botón es el numero `1`.  
Los id se pueden leer desde el puerto seriál.

La segunda línia son `0` o `1` a segunda del estado del botón.  
`1` significa empujado.

Un ejemplo de datos por 3 ESP, el primero con todos los botones apretados, el segundo ningun apretado, y el tercero solo tiene el primer boton apretado
```csv
1A3F22-1 1A3F22-2 1A3F22-3 3E4529-1 3E4529-2 3E4529-3 55E1AA-1 55E1AA-2 55E1AA-3
       1        1        1        0        0        0        1        0        0
```